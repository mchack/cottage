// SPDX-License-Identifier: ISC
package main

import (
	"fmt"
	"net"
	"net/http"
	"strings"
	"time"
)

// Resolve performs a DNS SRV lookup for the XMPP server that serves
// the given domain.
func resolve(domain string) (host string, port uint16, err error) {
	_, addrs, err := net.LookupSRV("xmpp-client", "tcp", domain)
	if err != nil {
		return "", 0, err
	}
	if len(addrs) == 0 {
		return "", 0, fmt.Errorf("No SRV records found for %v", domain)
	}

	return addrs[0].Target, addrs[0].Port, nil
}

func serverName(host string) string {
	return strings.Split(host, ":")[0]
}

func makeHandler(timeChan chan time.Time) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		timeChan <- time.Now()
	}
}
