// SPDX-License-Identifier: ISC
package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"
)

type State int

const (
	stateNormal = iota
	stateEmergency
	stateConnect
)

func handleTime(client Client, c chan time.Time, checkTime time.Duration, warningTime time.Duration, reminderTime time.Duration) {
	var emergencySentTime time.Time
	ticker := time.NewTicker(checkTime)

	lastTime := time.Now()
	state := stateConnect
	for {
		if state != stateConnect {
			select {
			case lastTime = <-c:
				fmt.Println("Cottage reported at", lastTime.Format(time.RFC3339))
			case t := <-ticker.C:
				fmt.Println("Tick at", t.Format(time.RFC3339))
			}
		}

		switch state {
		case stateNormal:
			if time.Since(lastTime) > warningTime {
				state = stateEmergency

				msg := fmt.Sprintf("EMERGENCY last heard of %v", lastTime.Format(time.RFC3339))
				if client.Send(msg) != nil {
					state = stateConnect
					break
				}
				emergencySentTime = time.Now()
			}

		case stateEmergency:
			if time.Since(lastTime) < warningTime {
				state = stateNormal

				msg := fmt.Sprintf("OK last heard %v", lastTime.Format(time.RFC3339))
				if client.Send(msg) != nil {
					state = stateConnect
					break
				}
			}

			if time.Since(emergencySentTime) > reminderTime {
				msg := fmt.Sprintf("EMERGENCY reminder last heard %v", lastTime.Format(time.RFC3339))
				if client.Send(msg) != nil {
					state = stateConnect
					break
				}
				emergencySentTime = time.Now()
			}

		case stateConnect:
			fmt.Printf("Connecting to XMPP server...\n")
			err := client.Connect()
			if err != nil {
				fmt.Printf("Connection to XMPP server failed\n")
				time.Sleep(10 * time.Second)
			} else {
				fmt.Printf("Connection to XMPP server succesful\n")
				state = stateNormal

				msg := fmt.Sprintf("CONNECTED %v, cottage last reported %v", time.Now().Format(time.RFC3339), lastTime.Format(time.RFC3339))
				if client.Send(msg) != nil {
					state = stateConnect
					break
				}
			}
		}
	}
}

func main() {
	var configFile = flag.String("config", "./.cottage.json", "Filename of configuration file")

	flag.Parse()
	conf, err := LoadConfig(*configFile)
	if err != nil {
		fmt.Printf("Couldn't load configuration file: %v\n", err)
		os.Exit(1)
	}

	fmt.Printf("conf: %#v\n", conf)

	client, err := NewClient(conf.Jid, conf.Password, conf.Destination, conf.SkipVerify)
	if err != nil {
		fmt.Printf("Couldn't connect to XMPP server: %v\n", err)
		os.Exit(1)
	}

	timeChan := make(chan time.Time)
	go handleTime(*client, timeChan, time.Duration(conf.CheckTime)*time.Minute, time.Duration(conf.WarningTime)*time.Minute, time.Duration(conf.ReminderTime)*time.Minute)

	fmt.Printf("Starting web server\n")

	http.HandleFunc("/", makeHandler(timeChan))

	//Use the default DefaultServeMux.
	err = http.ListenAndServe(conf.Listen, nil)
	if err != nil {
		fmt.Println("Web server exited: ", err)
		os.Exit(1)
	}
}
