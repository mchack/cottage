# cottage

A simple cottage health check web service that sends XMPP messages
when my cottage hasn't reported in.

The router in my cottage runs a small cron job that only curls an URL
on my web server every five minutes.

I created this program to track that the cottage still reports in now
and then and, if not, warns me about it by sending me an XMPP message.

The program expects a config file in `.cottage.json` looking something
like this:

```
{
    "listen": "127.0.0.1:8080",
    "jid": "your-automatic-friend@your.domain",
    "password": "secret",
    "skipverify": false,
    "destination": "your-own-xmpp-address@probably-on-your-cell-phone",
    "checktime": 15,
    "warningtime": 180,
    "remindertime": 180
}
```

It checks if the cottage (or whatever is supposed to hit the web
server) every `checktime` minutes. If nothing has been reported in
`warningtime` minutes it sends a warning over XMPP and reminds you
every `remindertime` minutes.

