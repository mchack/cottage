// SPDX-License-Identifier: ISC

package main

import (
	"crypto/tls"
	"fmt"
	"strings"

	"github.com/mattn/go-xmpp"
)

type Client struct {
	conn    *xmpp.Client
	options xmpp.Options
	remote  string
}

func NewClient(jid string, password Password, destination string, skipVerify bool) (*Client, error) {
	var client Client

	client.remote = destination

	tokens := strings.SplitN(jid, "@", 2)

	host, port, err := resolve(tokens[1])
	if err != nil {
		return nil, fmt.Errorf("Couldn't find a server to connect to.")
	}

	server := fmt.Sprintf("%v:%v", host, port)

	client.options = xmpp.Options{
		Host:                         server,
		User:                         jid,
		Password:                     string(password),
		NoTLS:                        true,
		StartTLS:                     true,
		InsecureAllowUnencryptedAuth: false,
	}

	xmpp.DefaultConfig = &tls.Config{
		ServerName:         serverName(server),
		InsecureSkipVerify: skipVerify,
	}

	return &client, nil
}

func (c *Client) Connect() error {
	var err error

	c.conn, err = c.options.NewClient()
	return err
}

func (c Client) Send(msg string) error {
	_, err := c.conn.Send(xmpp.Chat{Remote: c.remote, Type: "chat", Text: msg})
	return err
}
