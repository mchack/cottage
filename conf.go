// SPDX-License-Identifier: ISC
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Password string

func (p Password) String() string {
	return "<redacted>"
}

func (p Password) GoString() string {
	return "<redacted>"
}

type Config struct {
	Listen       string
	Jid          string
	Password     Password
	SkipVerify   bool
	Destination  string
	CheckTime    int
	WarningTime  int
	ReminderTime int
}

func LoadConfig(filename string) (Config, error) {
	c := Config{
		Listen:       "127.0.0.1:8080",
		Jid:          "",
		Password:     "",
		SkipVerify:   false,
		Destination:  "",
		CheckTime:    15,
		WarningTime:  180,
		ReminderTime: 180,
	}

	contents, err := ioutil.ReadFile(filename)
	if err != nil {
		return c, err
	}

	if err = json.Unmarshal(contents, &c); err != nil {
		return c, err
	}

	if c.Jid == "" {
		return c, fmt.Errorf("jid needs to be set to an XMPP JID like foo@example.org")
	}

	if c.Password == "" {
		return c, fmt.Errorf("password needs to be set")
	}

	if c.Destination == "" {
		return c, fmt.Errorf("destination needs to be set to an XMPP JID like foo@example.org")
	}

	return c, nil
}
